#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"ppmIO.h"
#include"commandUtil.h"
#include"imageManip.h"

int main (int argc,const char* argv[]) {
  //call commandUtil, where all the nitty-gritty is
  commandUtil(argc, argv);
  return 0;
}
