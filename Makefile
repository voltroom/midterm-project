# Define the compiler and flags

CC = gcc
CFLAGS = -std=c99 -Wall -Wextra -pedantic

# make project executable
project: project.o ppmIO.o commandUtil.o imageManip.o
	$(CC) -o project project.o ppmIO.o commandUtil.o imageManip.o

# make project.o (object file)
project.o: project.c ppmIO.h commandUtil.h imageManip.h
	$(CC) $(CFLAGS) -c project.c

# make ppmIO.o (object file)
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

# make commandUtil.o (object file)
commandUtil.o: commandUtil.c commandUtil.h
	$(CC) $(CFLAGS) -c commandUtil.c

# make imageManip.o (object file)
imageManip.o: imageManip.c imageManip.h ppmIO.h
	$(CC) $(CFLAGS) -c -lm imageManip.c

# make clean
clean:	rm -f *.o
	rm -f main

