#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageManip.h"

//function for invert
int Invert(Image* original, int tolerance, int red, int green, int blue) {
  //make it simple for us to type row and col numbers
  int row = original->rows;
  int col = original->cols;  
  int i;
  //these are constant component of given pixel
  const int redcol = 125;
  const int greencol = 161;
  const int bluecol = 170;
  //pixel pointer (hence the name pp)
  Pixel* pp;

  //check for red color difference between target and pixel component
  int reddif = redcol-red;
  //absolute value
  if(reddif < 0) {
    reddif = -reddif;
  }
  //check for green color difference between target and pixel component
  int greendif = greencol-green;
  //absolute value
  if(greendif < 0) {
    greendif = -greendif;
  }
  //check for blue color difference between target and pixel component
  int bluedif = bluecol-blue;
  //absolute value
  if(bluedif < 0) {
    bluedif = -bluedif;
  }
  //case for red
  if (reddif<=tolerance) {
    pp = original->data;
    for (i = 0; i < row * col; i++) {
      //flipping each bit
      pp->r = 255-(pp->r);
	pp++;
    }
  }
  //case for green
  if (greendif<=tolerance) {
    pp = original->data;
    for (i = 0; i < row * col; i++) {
      //flipping each bit
      pp->g = 255-(pp->g);
        pp++;
    }
  }

  //case for blue
  if (bluedif<=tolerance) {
    pp = original->data;
    for (i = 0; i < row * col; i++) {
      //flipping each bit
      pp->b = 255-(pp->b);
        pp++;
    }
  }

  return 0;
}

//function for reflect
int Reflect(Image* original) {
  //make it easy for us to type row and col
  int row = original->rows;
  int col = original->cols;
  int x, y;
  //swapping pixels needs to be done up to middleVal
  int middleVal = col/2;
  Pixel temp;
  //pixel pointer (hence the name pp)
  Pixel* pp;
  for (y = 0; y < row; y++) {
    //for loop for row
    pp = (original->data) + (col * y);
    for (x = 0; x < middleVal; x++) {
      //for loop for column
      //swapping for pixels begin
      temp = pp[x];
      pp[x] = pp[col-x-1];
      pp[col-x-1] = temp;
      
    }
  }
  return 0;
}

//function to crop an image                                                                                       
// row1, row2 are x-coordinates and col1, col2 are y coordinates                                                  
Image* Crop(Image* original, int col1, int row1, int col2, int row2) {
  //(r1,c1) is the top left pixel                                                                                 
  //(r2,c2) is the bottom right pixel                                                                             

  //allocate new memory with new rows and columns                                                                 
  int crop_row = row2 - row1;
  int crop_col = col2 - col1;

  Image* crop_image = (Image*)malloc(sizeof(Image));
  crop_image->rows = crop_row;
  crop_image->cols = crop_col;
  crop_image->data = (Pixel*)malloc(crop_image->rows * crop_image->cols* sizeof(Pixel));

  //new image with new boundary

  int a = 0; // new index
  int b = 0; // old index
  for (int i = row1; i < row2; i++) {
    for(int j = col1; j < col2; j++){
      b = i * original->cols + j;
      crop_image->data[a].r = original->data[b].r;
      crop_image->data[a].g = original->data[b].g;
      crop_image->data[a].b = original->data[b].b;
      a++;

    }
  }
  free(original->data);
  free(original);
  return crop_image;


}

//function for grayscale
int Grayscale(Image* original) {
  //make it easy for us to type row and col
  int row = original->rows;
  int col = original->cols;
  //count to keep track of the numbers so we return the original->data to initial point
  int count = 0;
  for (int i = 0; i < row * col; i++) {
    count++;
    //each intensity values
    double intensity, rintensity, gintensity, bintensity;
    //equation for grayscale given by the instructors
    rintensity = (double) original->data->r * 0.30;
    gintensity = (double) original->data->g * 0.59;
    bintensity = (double) original->data->b * 0.11;
    intensity = rintensity + gintensity + bintensity;
    //change each r, g, b to same intensity
    original->data->r = (unsigned char) intensity;
    original->data->g = (unsigned char) intensity;
    original->data->b = (unsigned char) intensity;
    original->data++;
  }
  //return the original->data to where it was at first
  original->data -= count;

  return 0;
  
}

//the helper function for changing the color                                                                               
unsigned char contrast_color(double adjust) {
  unsigned char	color;
  if (adjust > 0.5) {
    color = (unsigned char) 255;
  }//reach the max color
  else if (adjust < -0.5) {
    color = (unsigned char) 0;
  }//reach the min color
  else {
    color = (unsigned char)((adjust + 0.5) * 255);
  }
  return color;

}

int Contrast(Image* im, double contrast) {
  double adj_red;
  double adj_green;
  double adj_blue;

  for (int i = 0; i < im->rows * im->cols; i++) {
    adj_red = (double)(im->data[i].r)/255 - 0.5;
    adj_green = (double)(im->data[i].g)/255 - 0.5;
    adj_blue = (double)(im->data[i].b)/255 - 0.5;//set equally span 0

    adj_red *= contrast;
    adj_green *= contrast;
    adj_blue *= contrast;

    //adjusting rgb colors
    im->data[i].r = contrast_color(adj_red);
    im->data[i].g = contrast_color(adj_green);
    im->data[i].b = contrast_color(adj_blue);
    
  }
  return 0;


}
