#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include"ppmIO.h"
#include"commandUtil.h"
#include"imageManip.h"

int is_number (const char * s, int length, int dot) {
  for (int i = 0; i < length; i++) {
    if (!isdigit(s[i])) {
      if ((dot == 1) || (s[i] != '.')) {
	return 0;
      }
      else {
	dot = 1;
      }
    }
  }
  return 1;
}
//dot 1 test for integer , dot 2 test for decimals
//test if the userinput is a number 

//Read user input and return values from 0-8 according to the incorrect input
int commandUtil(int argc, const char* argv[]) {

  FILE* input = NULL;
  FILE* output = NULL;
  char argument[255];
  // not enought input for the program to run
  if(argc < 3) {
    fprintf(stderr, "Failed to supply input filename or output filename, or both\n");
    return 1;
  }
  else {
    input = fopen(argv[1], "rb");//open file and read in binary
    if(!input) {
      fprintf(stderr, "Specified input file could not be found\n");
      return 2;
    }
    output = fopen(argv[2], "wb");//output file created
    if(!output) {
      fclose(input);//close the file if there is no output file avalible
      fprintf(stderr, "Specified output file could not be opened for writing or writing output failed \n");
      return 4;
    }
    if(argc < 4) {
      fclose(input);
      fclose(output);//close input and output file if not argument is made
      fprintf(stderr, "No operation name was specified, or operation name specified was invalid\n");
      return 5;
    }
    //take the argument
    else {
      strcpy(argument, argv[3]);
    }
  }
  Image* im = readPPM(input);
  fclose(input);//close the file after reading

  
  if (im == NULL) {
    fclose(output);//close the output file if im failed
    printf("Specified input file is not a properly-formatted PPM file, or reading input somehow fails\n");
    return 3;
  }
  //start of invert
  if(strcmp(argument, "invert") == 0 && argc == 8){
    //test if the user inputs are integers 
    if (is_number(argv[4],strlen(argv[4]), 1) && is_number(argv[5],strlen(argv[5]), 1) && is_number(argv[6], strlen(argv[6]), 1) && is_number(argv[7], strlen(argv[7]), 1)){
      int tolerance = atoi(argv[4]);
      int red = atoi(argv[5]);
      int green = atoi(argv[6]);
      int blue = atoi(argv[7]);//assigning values
      if((tolerance >= 0) && (tolerance <= 255) && (red >= 0) && (red <= 255) && (green >= 0) && (green <= 255) && (blue >= 0) && (blue <= 255)) {
	Invert(im, tolerance, red, green, blue);
     
      }
    }
    else {
      free(im->data);
        free(im);
        fclose(output);
        fprintf(stderr, "Incorrect kind of arguments - please provide 4 integers for invert operation.\n");
	return 6;
    }
  }//invert  ends
  //start of reflect
  else if(strcmp(argument, "reflect") == 0) {
    if(argc == 4) {
      Reflect(im);
    }
    else {
      free(im->data);
      free(im);
      fclose(output);
      printf("Incorrect number of argument.\n");
      return 6;
    }
  }//reflect ends

  //start of grayscale
  else if (strcmp(argument, "grayscale") == 0) {
    if (argc == 4) {
      Grayscale(im);
    }
    else {
      free(im->data);
      free(im);
      fclose(output);
      printf("Incorrect number of arguments or king of arguments specified for the specified operation were provided.\n");
      return 6;
    }
  }//grayscale ends
  //start of crop
  else if (strcmp(argument, "crop") == 0) {
    // if there's enough commandline arguments
    if (argc == 8) {
     if (is_number(argv[4],strlen(argv[4]), 1) && is_number(argv[5],strlen(argv[5]), 1) && is_number(argv[6], strlen(argv[6]), 1) && is_number(argv[7], strlen(argv[7]), 1)) {
	int c1 = atoi(argv[4]);
	int r1 = atoi(argv[5]);
	int c2 = atoi(argv[6]);
	int r2 = atoi(argv[7]);//converting strings to integers
	if ((r1 < 0) || (r2 > im->rows) || (c1 < 0) || (c2 > im->cols) || (r2 < r1) || (c2 < c1)) {
	  free(im->data);
	  free(im);
	  fclose(output);
	  fprintf(stderr, "Arguments for crop operation were out of range for the given input image.\n");
	  return 7;
	}//test if the input coordinates make sense
	// when all requirements are met
	else {
	 im = Crop(im, c1, r1, c2, r2);
	}
      }
      else {
	free(im->data);
        free(im);
        fclose(output);
        fprintf(stderr, "Incorrect number of arguments or kind of arguments specified for the specified operation.\n");
	return 6;
      }
    }
    // if there's not enough commandline arguments
    else {
      free(im->data);
      free(im);
      fclose(output);
      fprintf(stderr, "Incorrect number of arguments or kind of arguments specified for the specified operation.\n");
      return 6;
    }
  }//end of crop operation

  //start of contrast
  else if (strcmp(argument, "contrast") == 0) {
    double contrast;
    // testing if there is correct commandline arguments
    if (argc == 5) {
      if (is_number(argv[4], strlen(argv[4]), 0)) {
	contrast = atof(argv[4]);
	Contrast(im, contrast);
      }
      else {
	free(im->data);
	free(im);
	fclose(output);
	printf("Incorrect number of arguments or kind of arguments specified for the specified operation.\n");
	return 6;
      }
    }
    // if commandline arguments is incorrect
    else {
      free(im->data);
      free(im);
      fclose(output);
      fprintf(stderr, "Incorrect number of arguments or kind of arguments specified for the specified operation.\n");
      return 6;
    }
  }//end of contrast

  //if there is no operation
  else {
    free(im->data);
    free(im);
    fclose(output);
    fprintf(stderr, "No operation was specified or operation name specified is invalid.\n");
    return 5;
  }
  //write output
  if (writePPM(output, im) == -1) {
    free(im->data);
    free(im);
    fclose(output);
    fprintf(stderr, "Specified output file could not be opened for writing, or writing output somehow fails.\n");
    return 4;
  }
  free(im->data);
  free(im);
  fclose(output);
  return 0;  
  
  
}
