#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ppmIO.h"


int Invert(Image* original, int tolerance, int red, int green, int blue);

int Reflect(Image* original);

Image* Crop(Image* original, int col1, int row1, int col2, int row2);

int Grayscale(Image* original);

int Contrast(Image* im, double contrast);

unsigned char contrast_color(double adjust);
