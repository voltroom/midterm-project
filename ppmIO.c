
//ppmIO.c
//601.220, Summer 2018
//Starter code for midterm project - feel free to edit/add to this file

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include "imageManip.h"

/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {
  
  char check[1000];
  char comment;
  Image* img;
  
  //read image format to check if it is ppm
  if (!fgets(check, sizeof(check), fp)) {
    printf("Image format is invalid. (Not ppm)");
    return NULL;
  }

  //if the format is ppm, the first 2 characters should be 'P6'
  if (check[0] != 'P' || check[1] != '6') {
    printf("Image format is invalid. (Not P6)");
    return NULL;
  }

  
  //skip the comment part
  fscanf(fp, "%c", &comment);
  if (comment == '#') {
    ungetc(comment, fp);
    fgets(check, sizeof(check), fp);
  } else {
    ungetc(comment, fp);
  }
  //allocate memory for image
  img = (Image*)malloc(sizeof(Image));

  if (!img) {
    printf("Allocating memory was not successful");
    return NULL;
  }
  
  //read image size information
  if (fscanf(fp, "%d %d", &img->cols, &img->rows) != 2) {
    printf("Invalid image size");
    return NULL;
  }

  int cols = img->cols;
  int rows = img->rows;

  //DEBUG
  printf("col = %d\n", cols);
  printf("rows = %d\n", rows);
  
  
   
  
  int max_color;
  fscanf(fp, "%d", &max_color);
  fgets(check, 1000, fp);

  if (max_color != 255) {
    free(img->data);
    free(img);
    return NULL;
  }

Pixel *pix = malloc(cols * rows * sizeof(Pixel));  
  
  //read pixel information
  int i;
  for (i = 0; i < cols * rows; i++) {
    fscanf(fp, "%c", &(pix->r));
    fscanf(fp, "%c", &(pix->g));
    fscanf(fp, "%c", &(pix->b));

    pix++;

    //DEBUG
    /*printf("%d ", pix->r);
    printf("%d ", pix->g);
    printf("%d ", pix->b);*/
    //printf("i = %d ", i);  
  }
  
  pix -= i;
  img->data = pix;

  
  return img;
}


/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }

  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  /* write pixels */
  int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    return -1;
  }

  /* success, so return number of pixels written */
  return num_pixels_written;
}



